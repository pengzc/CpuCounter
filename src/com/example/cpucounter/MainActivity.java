package com.example.cpucounter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	private Button button1; 
	private Button button2; 
	private Button button3; 
	private TextView textResult;
	private final int MSG_EXPORT	= 100;
	private final int MAX_LENTH		= 45;
	
    HashMap< String , Data> mProceessMap = new HashMap< String , Data>();
    Pattern mPattern = Pattern.compile(".*\\s(\\d+)%.*\\s(\\S+)");
	
    
	private Handler mHandler = new Handler() {
		
		@Override
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case MSG_EXPORT:
				//����map
				String s = export(mProceessMap);
				textResult.setText(s);
				break;
			default:
				break;
			}
		};
	};
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		init();
		button1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						while(true) {
							String result = null;
							try {
								String cmd[] = {"top", "-n", "1", "-m" ,"10"};
								result = ExcuteCommand.run(cmd, "/");
							} catch (IOException e1) {
								e1.printStackTrace();
							}
							StringTokenizer stoken = new StringTokenizer(result, "\n");
							int index = 0;
							while(stoken.hasMoreElements()) {
								String line = stoken.nextToken();
								if (index >= 3) {
									doAnalysis(line);
//									Log.i("test", "line " + index + "   " + line);
								}
								++index;
							}
							mHandler.sendEmptyMessage(MSG_EXPORT);
							
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				}).start();
			}
		});
		
		button2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String cmd[] = {"stop", "ril-daemon", };
				 try {
					 String result = ExcuteCommand.run(cmd, "/");
					 textResult.setText(result);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//				String s = export(mProceessMap);
//				textResult.setText(s);
//				
//				try {
//					FileOutputStream fs = new FileOutputStream("mnt/sdcard/cpu_counter_result.txt");
//					try {
//						fs.write(s.getBytes());
//						fs.close();
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
//				} catch (FileNotFoundException e) {
//					e.printStackTrace();
//				}
			}
		});
		
		button3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				/*
				 *       echo 8 > /sys/kernel/autohotplug/lock
      					 echo 1008000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq
      					 echo 1008000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
				 */
				String[] cmd1 = {"echo", "8", ">", "/sys/kernel/autohotplug/lock"}; 
				String[] cmd2 = {"echo", "1008000", ">", "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"}; 
				String[] cmd3 = {"echo", "1008000", ">", "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"}; 
				String s = null;
				try {
					s += ExcuteCommand.run(cmd1, "/");
					s +="\n";
					s += ExcuteCommand.run(cmd2, "/");
					s +="\n";
					s += ExcuteCommand.run(cmd3, "/");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				textResult.setText(s);
			}
		});
	}

	private void init() {
		button1 = (Button) findViewById(R.id.button1);
		button2 = (Button) findViewById(R.id.button2);
		button3 = (Button) findViewById(R.id.button3);
		
		textResult = (TextView) findViewById(R.id.text_result);
	}
	
	
	//   177  2  17% S    57 149384K  20964K  fg media    /system/bin/mediaserver
	private void doAnalysis(String line) {
		 Matcher result = mPattern.matcher(line);
		 if (result.matches()) {
//			 Log.i("test", "result    " + result.group(1) + result.group(2));
			 counter(result.group(2) ,Integer.valueOf(result.group(1)));
		 } else {
//			 Log.i("test", "failed");
		 }
	}
	
	private void counter(String pid, int percent) {
		if (!mProceessMap.containsKey(pid)) {
			mProceessMap.put(pid, new Data(percent));
		} else {
			++mProceessMap.get(pid).count;
			mProceessMap.get(pid).percent +=percent;
		}
	}
	
	private String export(HashMap< String , Data> map) {
		StringBuilder text = new StringBuilder();
		
		List<Map.Entry<String, Data>> datalist = sortHashMap(map);
		int keylenth = 0;
		for (Map.Entry<String, Data> entry : datalist) {
			String key = (String) entry.getKey();
			Data val = (Data) entry.getValue();
			keylenth = key.length();
			text.append(key);
			
			//����
			if (keylenth < MAX_LENTH) {
				for (int i = 0; i < (MAX_LENTH - keylenth); ++i) {
					text.append("  ");
				}
			}
			
			text.append( + val.percent / val.count + "%      " + val.count + "\n");
		}
		return text.toString();
	}
	
	private List<Map.Entry<String, Data>> sortHashMap(HashMap< String , Data> map) {
	    List<Map.Entry<String, Data>> list_Data = new ArrayList<Map.Entry<String, Data>>(map.entrySet());  
	    Collections.sort(list_Data, new Comparator<Map.Entry<String, Data>>()  
	      {   
	          public int compare(Map.Entry<String, Data> o1, Map.Entry<String, Data> o2)  
	          {  
	        	  float percent1 = o1.getValue().percent / o1.getValue().count;
	        	  float percent2 = o2.getValue().percent / o2.getValue().count;
	        	  float r = percent2 - percent1;
	        	  if (r > 0) return 1;
	        	  else if (r == 0) return 0;
	        	  else return -1;
	          }  
	      }); 
	    return list_Data;
	}
	
	private class Data {
		public int count = 0;
		public float percent = 0;
		
		Data(int percent) {
			this.percent = percent;
			this.count = 1;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
}
